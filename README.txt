Front End angular

Front-end básico desarrollado en Angular 9.1.7 

URL: http://localhost:8081/front-end-test-latam/


Repositorio git
https://mcallejasT@bitbucket.org/mcallejasT/front-end-angular.git

Intrucción para despliegue:

1) Prerrequisitos: Apache tomcat
2) Descargar las fuentes del repositorio git
3) Ejecutar sobre la carpeta base el comando: ng build
4) Copiar la carpeta generada an carpeta webapps del servidor Tomcat
5) Probar ingresando a la URL, modificando el host y el puerto según corresponda. 
