import { Component, Inject} from '@angular/core';
import { HttpClient, HttpParams,HttpHeaders} from '@angular/common/http';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { DatePipe } from '@angular/common'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front-end-test-latam';
  datePattern = '/^((0[1-9]|[12]\d|3[01])-(0[1-9]|1[0-2])-[12]\d{3})$/';
  personList: Person [] = [];
  newPerson: Person;
  angForm: FormGroup;

  constructor(private http: HttpClient,private fb: FormBuilder,public datepipe: DatePipe){
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       name: ['', Validators.required ],
       birthDate: ['', Validators.required ]
    });
  }

  openPoem(dialogPerson: Person): void {
    if(dialogPerson.poem != null){
      console.log("open Poem: " + dialogPerson.poem.url);
      let link = dialogPerson.poem.url;
      window.open(link, "_blank");
    }
  }

  addPerson(name:String,birthDate:String){

    let newDate =this.datepipe.transform(birthDate, 'dd-MM-yyyy');

    let params =  '?fullName=' + name + '&birthDate=' +newDate;

    this.http.get<Person>("/birthday-rest/api/getBirthday" + params)
    .subscribe(data=>this.addNewPerson(data), error => this.addError())

  };

  addError(){
    let newPerson: Person = new Person;
    newPerson.name = 'XXXXX';
    newPerson.surname = 'XXXXX';
    newPerson.age=999;
    newPerson.message="Falla de servicio Rest, reinicie y actualice la página.";
    this.personList.push(newPerson) 
  }

  addNewPerson(newPerson: Person){
    let zero:Number=0

    this.personList.push(newPerson) 
    this.newPerson=newPerson

    if(newPerson.daysTillBirthday == zero){
      newPerson.message="FELICIDADES EN TU CUMPLEAÑOS N° " + newPerson.age + ", presiona al lado para ver el poema que te dedicamos."
      newPerson.enablePoem=true
      newPerson.poemButtonLabel = newPerson.poem.title
    }else{
      newPerson.message= "Faltan " + newPerson.daysTillBirthday + " días para tu cumpleaños, no hay poema para ti ;)"
      newPerson.poemButtonLabel = 'No hay poema'
    }
    this.createForm()
    console.log(newPerson);
  }
}

export class Person {
	name: String;
	surname: String;
	age:  Number;
  birthDate: String;
  newBirthDate: String;
	daysTillBirthday: Number;
  poem: Poem;
  enablePoem:Boolean = false;
  message: String;
  poemButtonLabel:String;
  }

class Poem {
    title: String;
    content: String;
    url: string;
    poet: Poet;
  }

  class Poet {
    name: String;
    url: String;
  }

